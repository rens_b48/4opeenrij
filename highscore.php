

<div id="wrapper">

<?php

	$aantal=countUsers();

	echo <<<EOT
	<h2>Highscores</h2>

	<p>Aantal geregistreerde gebruikers: {$aantal}</p>
	
	<div class="tabelopmaak">
		<h2>All time top 10</h2>
			<div class="scorehead">
			<table>
				<tr>
				    <th>Gebruikersnaam</th>
				    <th>Gewonnen</th>
				    <th>Verloren</th> 
				    <th>Gelijkspel</th> 
				 </tr>
EOT;

	$row=getHigh();				
	foreach ($row as $show) { // Shoutout naar Eric! 
		echo <<<EOT
	  <tr>
	    <td>{$show['gebruikersnaam']}</td> 
	    <td>{$show['gewonnen']}</td>
	    <td>{$show['verloren']}</td> 
	    <td>{$show['gelijk']}</td> 
	  </tr>
EOT;
	}

echo <<<EOT
	</table>
	</div>
		</div>
EOT;

?>

</div>