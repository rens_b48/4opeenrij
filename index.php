<?php include 'header.php'; ?>

<div id="wrapper">
			<?php
			include("functions.php");
			$page = empty($_GET['page']) ? '' : $_GET['page'];
			switch($page)
			{	
				case "inloggen":
					$page = 'inlog.php';
					break;		
				case "highscore":
					$page = 'highscore.php';
					break;
				case 'spel':
					$page = 'spel.php';
					break;
				case 'profiel':
					$page = 'profile.php';
					break;
				case 'uitloggen':
					$page = 'logout.php';
					break;
				case 'registreer':
					$page = 'registreer.php';
					break;
				default:
					$page = 'inlog.php';
					break;
			}
			require_once($page);
		?>
</div>

<?php include 'footer.php'; ?>
