<!DOCTYPE html>
<html>
<head>
	<title>Vier op een rij</title>
	<link href="stylesheet.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href='https://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
</head>

<?php session_start();?>
<?php if(isset($_SESSION['username'])) : ?>
	<div class="menu">
			<div id='cssmenu'>
				<ul>
				   <li><a href='index.php?page=profiel'><span>Profiel</span></a></li>
				   <li><a href='index.php?page=spel'><span>Game</span></a></li>
				   <li><a href='index.php?page=highscore'><span>Highscores</span></a></li>
				   <li><a href='index.php?page=uitloggen'><span>Uitloggen</span></a></li>
				</ul>
			</div>
		</div>
		
	<?php else: ?>

		<div class="menu">
			<div id='cssmenu'>
				<ul>
				   <li><a href='index.php'><span>Inloggen</span></a></li>
				   <li><a href='index.php?page=highscore'><span>Highscores</span></a></li>
				   <li><a href='index.php?page=registreer'><span>Registreren</span></a></li>
				</ul>
			</div>
		</div>

	<?php endif; ?>