<?php


	if (mb_strlen($email)<1) {
		$processing=false;
		$foutmeldingen[]="Vul een email adres in.<br>";
	} else {
		if (mb_strlen($email)>254) {
			$processing=false;
			$foutmeldingen[]="Email adres mag niet groter zijn dan 254 tekens.<br>";
		}
		if ($email==emailIsPresent($email)) {
			$verwerken=false;
			$foutmeldingen[]="De email bestaat al";
		}
		if (mb_strlen($email)<7) {
			$processing=false;
			$foutmeldingen[]="Email is te kort.<br>";
		}
		$location_at = strpos($email, "@");
		if (strpos($email,'@') == false) {
		    $processing=false;
		    $foutmeldingen[]="Email adres klopt niet.<br>";
		}
		if (mb_strlen($email)<$location_at) {
			$processing=false;
			$foutmeldingen[]="Email adres klopt niet.<br>";
		}
		if ($location_at>65) {
			$processing=false;
			$foutmeldingen[]="Email adres klopt niet.<br>";
		}
		if (($email-$location_at)>255) {
			$processing=false;
			$foutmeldingen[]="Email adres klopt niet.<br>";
		}
	}
	

?>
