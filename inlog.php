
	<div id="wrapper">
		<div id="login">
<?php

	if(isset($_SESSION['username'])) {
		header("Location:index.php?page=highscore");
	}
	
	$foutmeldingen = array ();
	
	if (empty($_POST)) {
		$verwerken=false;
		$verwerken_naam1=false;
		$verwerken_naam2=false;
		
		$name_one = $password_one = null;
		$name_two = $password_two = null;
	}else{
		$verwerken=true;
		$name_one=($_POST["naam_een"]);
		$password_one=($_POST["wachtwoord_een"]);

		$name_two=($_POST["naam_twee"]);
		$password_two=($_POST["wachtwoord_twee"]);
		
		
		if (mb_strlen($name_one)<1) {
			$verwerken=false;

		}
		if (mb_strlen($name_one)>15) {
			$verwerken=false;

		}
		if (mb_strlen($password_one)<1) {
			$verwerken=false;
	
		}
		if (mb_strlen($password_one)>254) {
			$verwerken=false;
		
		}

		if (mb_strlen($name_two)<1) {
			$verwerken=false;
			
		}
		if (mb_strlen($name_two)>15) {
			$verwerken=false;
		
		}
		if (mb_strlen($password_two)<1) {
			$verwerken=false;
			
		}
		if (mb_strlen($password_two)>254) {
			$verwerken=false;
			
		}

		if(getLogin($name_one, $password_one)>0) {
			$verwerken_naam1=true;
		}else{
			$verwerken_naam1=false;
			$verwerken=false;
			$foutmeldingen[]="Invoer speler 1 incorrect";
		}

		if(getLogin($name_two, $password_two)>0){
			$verwerken_naam2=true;
		}else{
			$verwerken_naam2=false;
			$verwerken=false;
			$foutmeldingen[]="Invoer speler 2 incorrect";
		}

		if(($verwerken_naam1) && ($verwerken_naam2)==true){
			$verwerken=true;
			$_SESSION['username']=$name_one;
			$_SESSION['username_two']=$name_two;

			header("Location:index.php?page=profiel");
		}
		$verzend=empty($foutmeldingen);
	}
	
	if($verwerken==true){

EOT;
	}else{
		
	$verwerken=false;
	echo <<<EOT

	<h2>Inloggen</h2>	
		<form method="POST">
			<p>Gebruiker 1</p>
			<input type="text" id="name" name="naam_een" value="{$name_one}" placeholder="Gebruikersnaam"/>
			<input type="password" id="password" name="wachtwoord_een" value="{$password_one}" placeholder="Wachtwoord" />

			<p>Gebruiker 2</p>
			<input type="text" id="name" name="naam_twee" value="{$name_two}" placeholder="Gebruikersnaam"/>
			<input type="password" id="password" name="wachtwoord_twee" value="{$password_two}" placeholder="Wachtwoord" />

			<input type="submit" value="Inloggen">
		</form>
		<div id="form_links"><a href="index.php?page=registreer">Nog geen account?</a></div>
	</div>

EOT;
	foreach($foutmeldingen as $foutmelding) {echo <<<EOT
		<div class="foutmelding">
			{$foutmelding} 
		</div>
EOT;
		}
	}
?>
</div>
</div>