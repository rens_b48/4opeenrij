<?php
	
	include('db.php');
	include('config.php');

	//functie check username
	//functie check horizontaal
	//functie check verticaal
	
	function getLogin($name, $password) {
		$mysqli=DB::get();
		$name=$mysqli->real_escape_string($name);
		$password=$mysqli->real_escape_string($password);
		$result=$mysqli->query("SELECT * FROM gebruikers WHERE gebruikersnaam = '$name' AND password = '$password'");
		$check = mysqli_num_rows($result);
		return($check);
	}

	function emailIsPresent($email) {
		$mysqli=DB::get();
		$name=$mysqli->real_escape_string($email);
		$result=$mysqli->query("
			SELECT email FROM gebruikers WHERE email='{$email}'
		");
		$row=$result->fetch_row();
		return $row[0];
	}
	function usernameIsPresent($username) {
		$mysqli=DB::get();
		$name=$mysqli->real_escape_string($username);
		$result=$mysqli->query("
			SELECT gebruikersnaam FROM gebruikers WHERE gebruikersnaam='{$username}'
		");
		$row=$result->fetch_row();
		return $row[0];
	}

	function countUsers() {
		$mysqli=DB::get();
		$count_reacties = $mysqli->query("SELECT count(id) AS aantal FROM gebruikers");
		$row=$count_reacties->fetch_row();
		return ($row[0]);
	}

	function getHigh() {
		$mysqli=DB::get();

		$result = $mysqli->query("SELECT * FROM highscores
		JOIN gebruikers ON gebruikers.id=highscores.gebruiker_id
		ORDER BY gewonnen DESC, gelijk DESC, verloren DESC LIMIT 10");
		
		$highscores = [];

		while ($row=$result->fetch_assoc()) {
			$highscores[]=$row;
		}
		return $highscores;
	}

	function getRegister($name, $email, $password) {
		$mysqli=DB::get();
		$name=$mysqli->real_escape_string($name);
		$email=$mysqli->real_escape_string($email);
		$password=$mysqli->real_escape_string($password);

		$result=$mysqli->query(<<<EOT
		INSERT INTO gebruikers (gebruikersnaam, email, password)
		VALUES ("{$name}", "{$email}", "{$password}")
EOT
		);

		$result2=$mysqli->query(<<<EOT
		SELECT id FROM gebruikers WHERE gebruikersnaam='{$name}'
EOT
		);

		$id=$result2->fetch_row();

		$result3=$mysqli->query(<<<EOT
		INSERT INTO highscores (gebruiker_id, gewonnen, verloren, gelijk)
		VALUES ({$id[0]}, 0, 0, 0)
EOT
		);
		return(true);
	}

	function valid_email($email) {
        // First, we check that there's one @ symbol, and that the lengths are right
        if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
            // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
            return false;
        }
        // Split it into sections to make life easier
        $email_array = explode("@", $email);
        $local_array = explode(".", $email_array[0]);
        for ($i = 0; $i < sizeof($local_array); $i++) {
            if (!preg_match("/^(([A-Za-z0-9!#$%&'*+\/=?^_`{|}~-][A-Za-z0-9!#$%&'*+\/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i])) {
                return false;
            }
        }
        if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
            $domain_array = explode(".", $email_array[1]);
            if (sizeof($domain_array) < 2) {
                return false; // Not enough parts to domain
            }
            for ($i = 0; $i < sizeof($domain_array); $i++) {
                if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
                    return false;
                }
            }
        }

        return true;
    }

    function get_bord($id) {
		$mysqli=DB::get();
		$result=$mysqli->query("select DISTINCT rij, kolom , player FROM zetten where id = {$id}");
		while ($row=$result->fetch_row()) {
			$speelveld[$row[0]][$row[1]] = $row[2];
		}
		if(!empty($speelveld)){
			return($speelveld);
		}	
	}
	function query_invoegen($querystring) {
		$mysqli=DB::get();
		$result=$mysqli->query($querystring);
	}

	function getMaxBoardId() {
		$mysqli=DB::get();

		$result=$mysqli->query(<<<EOT
		SELECT MAX(id) FROM zetten
EOT
		);
		$row=$result->fetch_row();
		return($row[0]);
	}

	function clearBoard($id) {
		$mysqli=DB::get();
		$id=$mysqli->real_escape_string($id);

		$result=$mysqli->query(<<<EOT
		DELETE FROM zetten WHERE id={$id}
EOT
		);
		return(true);
	}

	function checkWinner($speelveld, $beurt,$naam_beurt) {
		//checking vertical winner
		for($r=6; $r>3; $r--) {
			for($c=1; $c<=7; $c++) {
				if($speelveld[$r][$c] == $beurt && $speelveld[$r-1][$c] == $beurt && $speelveld[$r-2][$c] == $beurt && $speelveld[$r-3][$c] == $beurt){
					return("{$naam_beurt} heeft gewonnen"); 
				}
			}
		}

		//checking horizontal winner
		for($r=6; $r>0; $r--) {
			for($c=1; $c<=4; $c++){
				if($speelveld[$r][$c] == $beurt && $speelveld[$r][$c+1] == $beurt && $speelveld[$r][$c+2] == $beurt && $speelveld[$r][$c+3] == $beurt){
					return("{$naam_beurt} heeft gewonnen");
				}
			}
		}

		//checking diagnal winner1
		for($r=6; $r>=3; $r--) {
			for($c=1; $c<=4; $c++){
				if($speelveld[$r][$c] == $beurt && $speelveld[$r-1][$c+1] == $beurt && $speelveld[$r-2][$c+2] == $beurt && $speelveld[$r-3][$c+3] == $beurt){ return("{$naam_beurt} heeft gewonnen");
				}
			}
		}

		//checking diagnal winner2
		for($r=1; $r<=4; $r++) {
			for($c=1; $c<=4; $c++){
				if($speelveld[$r][$c] == $beurt && $speelveld[$r+1][$c+1] == $beurt && $speelveld[$r+2][$c+2] == $beurt && $speelveld[$r+3][$c+3] == $beurt){
					return("{$naam_beurt} heeft gewonnen");
				}
			}
		}

	}

	function setWinner($user_id) {
		$mysqli=DB::get();
		$user_id=$mysqli->real_escape_string($user_id);

		$result=$mysqli->query(<<<EOT
		SELECT gebruiker_id FROM highscores WHERE gebruiker_id={$user_id}
EOT
		);

		if($result && mysqli_num_rows($result)>0){
			//update query
			$result2=$mysqli->query(<<<EOT
			UPDATE highscores SET gewonnen=gewonnen+1
			WHERE gebruiker_id={$user_id}
EOT
			);
		} else {
			//insert query
			$result3=$mysqli->query(<<<EOT
			INSERT INTO highscores (gebruiker_id, gewonnen, verloren, gelijk) VALUES ({$user_id},1, 0, 0)
EOT
			);
		}

		return(true);
	}

	function setLoser($user_id) {
		$mysqli=DB::get();
		$user_id=$mysqli->real_escape_string($user_id);

		$result=$mysqli->query(<<<EOT
		SELECT gebruiker_id FROM highscores WHERE gebruiker_id={$user_id}
EOT
		);

		if($result && mysqli_num_rows($result)>0){
			//update query
			$result2=$mysqli->query(<<<EOT
			UPDATE highscores SET verloren=verloren+1
			WHERE gebruiker_id={$user_id}
EOT
			);
		} else {
			//insert query
			$result3=$mysqli->query(<<<EOT
			INSERT INTO highscores (gebruiker_id, gewonnen, verloren, gelijk) VALUES ({$user_id},0, 1, 0)
EOT
			);
		}

		return(true);
	}

	function getUser($username) {
		$mysqli=DB::get();
		$username=$mysqli->real_escape_string($username);

		$result=$mysqli->query(<<<EOT
		SELECT id FROM gebruikers
		WHERE gebruikersnaam='{$username}'
EOT
		);
		$row=$result->fetch_row();
		return($row[0]);
	}

?>