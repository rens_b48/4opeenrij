-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- Machine: localhost
-- Genereertijd: 26 jan 2016 om 09:59
-- Serverversie: 5.6.13
-- PHP-versie: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `4opeenrij`
--
CREATE DATABASE IF NOT EXISTS `4opeenrij` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `4opeenrij`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `bord`
--

CREATE TABLE IF NOT EXISTS `bord` (
  `gebruiker_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `rij` int(11) NOT NULL,
  `kolom` int(11) NOT NULL,
  `waarde` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Gegevens worden uitgevoerd voor tabel `bord`
--

INSERT INTO `bord` (`gebruiker_id`, `id`, `rij`, `kolom`, `waarde`) VALUES
(0, 1, 1, 1, 1),
(0, 1, 1, 2, 0),
(0, 1, 1, 3, 1),
(0, 1, 1, 4, 0),
(0, 1, 1, 5, 0),
(0, 1, 1, 6, 0),
(0, 1, 1, 7, 0),
(0, 1, 1, 8, 0),
(0, 1, 2, 1, 1),
(0, 1, 2, 2, 0),
(0, 1, 2, 3, 1),
(0, 1, 2, 4, 0),
(0, 1, 2, 5, 0),
(0, 1, 2, 6, 0),
(0, 1, 2, 7, 0),
(0, 1, 2, 8, 2),
(0, 1, 3, 1, 1),
(0, 1, 3, 2, 0),
(0, 1, 3, 3, 1),
(0, 1, 3, 4, 0),
(0, 1, 3, 5, 0),
(0, 1, 3, 6, 0),
(0, 1, 3, 7, 1),
(0, 1, 3, 8, 2),
(0, 1, 4, 1, 1),
(0, 1, 4, 2, 0),
(0, 1, 4, 3, 2),
(0, 1, 4, 4, 0),
(0, 1, 4, 5, 0),
(0, 1, 4, 6, 2),
(0, 1, 4, 7, 2),
(0, 1, 4, 8, 1),
(0, 1, 5, 1, 1),
(0, 1, 5, 2, 0),
(0, 1, 5, 3, 1),
(0, 1, 5, 4, 0),
(0, 1, 5, 5, 1),
(0, 1, 5, 6, 1),
(0, 1, 5, 7, 1),
(0, 1, 5, 8, 2),
(0, 1, 6, 1, 1),
(0, 1, 6, 2, 2),
(0, 1, 6, 3, 2),
(0, 1, 6, 4, 2),
(0, 1, 6, 5, 2),
(0, 1, 6, 6, 1),
(0, 1, 6, 7, 1),
(0, 1, 6, 8, 2),
(0, 1, 7, 1, 2),
(0, 1, 7, 2, 1),
(0, 1, 7, 3, 2),
(0, 1, 7, 4, 1),
(0, 1, 7, 5, 2),
(0, 1, 7, 6, 1),
(0, 1, 7, 7, 2),
(0, 1, 7, 8, 1),
(0, 1, 8, 1, 1),
(0, 1, 8, 2, 2),
(0, 1, 8, 3, 1),
(0, 1, 8, 4, 2),
(0, 1, 8, 5, 1),
(0, 1, 8, 6, 2),
(0, 1, 8, 7, 1),
(0, 1, 8, 8, 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `bord_lokaal`
--

CREATE TABLE IF NOT EXISTS `bord_lokaal` (
  `gebruiker_id` int(11) NOT NULL,
  `bord_id` int(11) NOT NULL,
  `rij` int(11) NOT NULL,
  `kolom` int(11) NOT NULL,
  `waarde` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `gebruikers`
--

CREATE TABLE IF NOT EXISTS `gebruikers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gebruikersnaam` varchar(20) COLLATE utf8_bin NOT NULL,
  `password` varchar(20) COLLATE utf8_bin NOT NULL,
  `email` varchar(254) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Gegevens worden uitgevoerd voor tabel `gebruikers`
--

INSERT INTO `gebruikers` (`id`, `gebruikersnaam`, `password`, `email`) VALUES
(1, 'Rens', '123', 'renskrauweel@ziggo.nl'),
(2, 'Nils', '123', 'renskrauweel@ziggo.nl');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `highscores`
--

CREATE TABLE IF NOT EXISTS `highscores` (
  `gebruiker_id` int(11) NOT NULL,
  `gewonnen` int(11) NOT NULL,
  `verloren` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `spelers_lokaal`
--

CREATE TABLE IF NOT EXISTS `spelers_lokaal` (
  `bord_id` int(11) NOT NULL,
  `gebruiker_id` int(11) NOT NULL,
  `speler1` int(11) NOT NULL,
  `speler2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
