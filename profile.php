<?php
	if(!isset($_SESSION['username'])) {
		header("Location:index.php");
	}
?>

	<div id="wrapper">
	<h2>Profiel</h2>
	<p>U bent nu ingelogd als  <?php echo ($_SESSION['username']) ?> en <?php echo ($_SESSION['username_two']) ?>. </p>
	<p>Het persoonlijk overzicht: </p>

	<div class="tabelopmaak">
	<div class="scorehead">
	<table><tr>
	    <th>Gebruikersnaam</th>
	    <th>Gewonnen</th> 
	    <th>Verloren</th>
	    <th>Gelijkspel</th> 
	 </tr>

<?php

	$mysqli=DB::get();
	$myScore = $mysqli->query("
		SELECT * FROM gebruikers
		JOIN highscores ON highscores.gebruiker_id=gebruikers.id
		WHERE gebruikersnaam = '{$_SESSION['username']}'
		");
	while ($row=$myScore ->fetch_assoc()){
		echo <<<EOT
			  <tr>
			    <td>{$row['gebruikersnaam']}</td>
			    <td>{$row['gewonnen']}</td> 
			    <td>{$row['verloren']}</td>
			    <td>{$row['gelijk']}</td> 
			  </tr>
EOT;
}

	$myScore2 = $mysqli->query("
		SELECT * FROM gebruikers
		JOIN highscores ON highscores.gebruiker_id=gebruikers.id
		WHERE gebruikersnaam = '{$_SESSION['username_two']}'
		");
	while ($row=$myScore2 ->fetch_assoc()){
		echo <<<EOT
			  <tr>
			    <td>{$row['gebruikersnaam']}</td>
			    <td>{$row['gewonnen']}</td> 
			    <td>{$row['verloren']}</td>
			    <td>{$row['gelijk']}</td> 
			  </tr>
EOT;
}
echo <<<EOT
	</table>
	</div>
		</div>
EOT;

?>
	</div>