-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 19 feb 2016 om 15:18
-- Serverversie: 10.1.8-MariaDB
-- PHP-versie: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vieropeenrij`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `beurten`
--

CREATE TABLE `beurten` (
  `spel_id` int(11) NOT NULL,
  `beurt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `gebruikers`
--

CREATE TABLE `gebruikers` (
  `id` int(11) NOT NULL,
  `gebruikersnaam` varchar(20) COLLATE utf8_bin NOT NULL,
  `password` varchar(20) COLLATE utf8_bin NOT NULL,
  `email` varchar(254) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Gegevens worden geëxporteerd voor tabel `gebruikers`
--

INSERT INTO `gebruikers` (`id`, `gebruikersnaam`, `password`, `email`) VALUES
(1, 'Rens', '123', 'renskrauweel@ziggo.nl'),
(2, 'Nils', '123', 'renskrauweel@ziggo.nl'),
(3, 'test', '123', 'fjehfeskjfh@hkjfdshfkds.vbl'),
(4, 'Eric', 'eric', 'fuck@off.now'),
(5, 'Kees', 'hello', 'kees@hotmail.com'),
(8, 'Henkie', '123', 'henkie@henk.henk'),
(9, 'Robert', '123', 'roberkrauweel@ziggo.nl');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `highscores`
--

CREATE TABLE `highscores` (
  `gebruiker_id` int(11) NOT NULL,
  `gewonnen` int(11) NOT NULL DEFAULT '0',
  `verloren` int(11) NOT NULL DEFAULT '0',
  `gelijk` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Gegevens worden geëxporteerd voor tabel `highscores`
--

INSERT INTO `highscores` (`gebruiker_id`, `gewonnen`, `verloren`, `gelijk`) VALUES
(1, 53, 5, 5),
(2, 6, 1, 0),
(3, 2, 1, 0),
(4, 5, 2, 0),
(5, 5, 2, 0),
(8, 7, 0, 0),
(9, 1, 0, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `spelers_lokaal`
--

CREATE TABLE `spelers_lokaal` (
  `bord_id` int(11) NOT NULL,
  `gebruiker_id` int(11) NOT NULL,
  `speler1` int(11) NOT NULL,
  `speler2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `spellen`
--

CREATE TABLE `spellen` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `winnaar` int(11) NOT NULL,
  `datum_spel` datetime NOT NULL,
  `lengte_spel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `zetten`
--

CREATE TABLE `zetten` (
  `id` int(11) NOT NULL,
  `rij` int(11) NOT NULL,
  `kolom` int(11) NOT NULL,
  `player` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Gegevens worden geëxporteerd voor tabel `zetten`
--

INSERT INTO `zetten` (`id`, `rij`, `kolom`, `player`) VALUES
(1, 1, 1, 0),
(1, 1, 2, 0),
(1, 1, 3, 0),
(1, 1, 4, 0),
(1, 1, 5, 0),
(1, 1, 6, 0),
(1, 1, 7, 0),
(1, 2, 1, 0),
(1, 2, 2, 0),
(1, 2, 3, 0),
(1, 2, 4, 0),
(1, 2, 5, 0),
(1, 2, 6, 0),
(1, 2, 7, 0),
(1, 3, 1, 1),
(1, 3, 2, 2),
(1, 3, 3, 0),
(1, 3, 4, 0),
(1, 3, 5, 0),
(1, 3, 6, 0),
(1, 3, 7, 0),
(1, 4, 1, 2),
(1, 4, 2, 1),
(1, 4, 3, 2),
(1, 4, 4, 0),
(1, 4, 5, 0),
(1, 4, 6, 0),
(1, 4, 7, 0),
(1, 5, 1, 1),
(1, 5, 2, 2),
(1, 5, 3, 1),
(1, 5, 4, 2),
(1, 5, 5, 0),
(1, 5, 6, 0),
(1, 5, 7, 0),
(1, 6, 1, 2),
(1, 6, 2, 1),
(1, 6, 3, 2),
(1, 6, 4, 1),
(1, 6, 5, 1),
(1, 6, 6, 0),
(1, 6, 7, 0);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `gebruikers`
--
ALTER TABLE `gebruikers`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `highscores`
--
ALTER TABLE `highscores`
  ADD PRIMARY KEY (`gebruiker_id`);

--
-- Indexen voor tabel `spellen`
--
ALTER TABLE `spellen`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `gebruikers`
--
ALTER TABLE `gebruikers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT voor een tabel `highscores`
--
ALTER TABLE `highscores`
  MODIFY `gebruiker_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT voor een tabel `spellen`
--
ALTER TABLE `spellen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
