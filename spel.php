<?php
	if(!isset($_SESSION['username'])) {
		header("Location:index.php?page=inloggen");
	}
?>

	<div id="wrapper">
		<h2>Vier op een rij</h2>
	<?php

	if (!empty($_GET['status']) && ($_GET['status'])=='quit') {
		setcookie('isPlaying', true, time() - 1300, "/");
		// functie om database leeg te maken
		$id=getMaxBoardId()[0];
		clearBoard($id);
		echo "<div class='spel_gestopt'>";
		echo "<p>Je hebt het spel gestopt</p>";
		echo "<a href='index.php?page=spel'>Nog een keer spelen?</a>";
		echo "</div>";
	} else {
		if(!isset($_COOKIE['isPlaying'])) {
		    setcookie('isPlaying', true, time() + (1200), "/");
		    /* Je bent nu aan het spelen. */
		}

		
//**********************		The game	  ************************//

		$rij   = 6;
		$kolom = 8;
		$id = $id=getMaxBoardId()[0];
		if (empty($id)) {
			$id=1;
		}
		$beurt=1;
		$speelveld = get_bord($id);
		if (empty($_GET['gewonnen'])) {
			$gewonnen=false;
		} else {
			$gewonnen=$_GET['gewonnen'];
		}

		if(empty($speelveld)) {
			for ($teller = 1; $teller <=6; $teller++) {
				for ($teller2 = 1; $teller2 <=7; $teller2++) {
					$speelveld[$teller][$teller2] = 0;
					query_invoegen("
						INSERT INTO `zetten`(`id`, `rij`, `kolom`, `player`) VALUES ({$id},{$teller},{$teller2},{$speelveld[$teller][$teller2]})
						");
				}
			}
		}

		if (empty($_GET['kolom']) && empty($_GET['winner'])) {
			echo "<div class='speler_beurt'>" . ($_SESSION['username']) . " heeft de beurt!</div>";
		}

		if (!empty($_GET['kolom'])) {
			$input = $_GET['kolom'];
			if (!empty($_GET['beurt'])) {
				$beurt=$_GET['beurt'];
				if ($beurt==1) {
					$beurt=2;
					echo "<div class='speler_beurt'>" . ($_SESSION['username_two']) . " heeft de beurt!</div>";
				} elseif ($beurt==2) {
					$beurt=1;
					echo "<div class='speler_beurt'>" . ($_SESSION['username']) . " heeft de beurt!</div>";
				}
			}
			
			
			if ($speelveld[6][$input] == 0) {
				$speelveld[6][$input]=$beurt;
				query_invoegen("UPDATE zetten set player = {$speelveld[6][$input]} WHERE id= {$id} and rij = 6 and kolom = {$input}");
			} else {
				if ($speelveld[5][$input] == 0) {
					$speelveld[5][$input]=$beurt;
					query_invoegen("UPDATE zetten set player = {$speelveld[5][$input]} WHERE id= {$id} and rij = 5 and kolom = {$input}");
				} else {
					if ($speelveld[4][$input] == 0) {
						$speelveld[4][$input]=$beurt;
						query_invoegen("UPDATE zetten set player = {$speelveld[4][$input]} WHERE id= {$id} and rij = 4 and kolom = {$input}");
					} else {
						if ($speelveld[3][$input] == 0) {
							$speelveld[3][$input]=$beurt;
							query_invoegen("UPDATE zetten set player = {$speelveld[3][$input]} WHERE id= {$id} and rij = 3 and kolom = {$input}");
						} else {
							if ($speelveld[2][$input] == 0) {
								$speelveld[2][$input]=$beurt;
								query_invoegen("UPDATE zetten set player = {$speelveld[2][$input]} WHERE id= {$id} and rij = 2 and kolom = {$input}");
							} elseif ($speelveld[1][$input] == 0) {
								$speelveld[1][$input]=$beurt;
								query_invoegen("UPDATE zetten set player = {$speelveld[1][$input]} WHERE id= {$id} and rij = 1 and kolom = {$input}");
							}
						}
					}
				}
			}
			//Checken op winnaar
			if ($beurt==1) {
				$naam_beurt=$_SESSION['username_two'];
			} elseif ($beurt==2) {
				$naam_beurt=$_SESSION['username'];
			}

			$victoryString=checkWinner($speelveld, $beurt,$naam_beurt);
			if (!empty($victoryString)) {
				$winnerId=intval(getUser($naam_beurt));
				setWinner($winnerId);
				if ($beurt==1) {
					$loser=$_SESSION['username'];
				} else {
					$loser=$_SESSION['username_two'];
				}
				$loser=intval(getUser($loser));
				setLoser($loser);
				header("Location:index.php?page=spel&winner={$naam_beurt}");
			}
		}

		if (empty($_GET['winner'])) {
			echo "<table class='spelbord'>";
			for ($teller = 1; $teller <=6; $teller++) {
				echo "<tr>";
				for ($teller2 = 1; $teller2 <=7; $teller2++) {
					echo "<td width=50><a href='index.php?page=spel&kolom=$teller2&beurt=$beurt'><img style='border-radius: 50%' 
					src='".$speelveld[$teller][$teller2].".png' /></a></td>";
				}
				echo "</tr>";
			}
			echo "</table>";
		} else {
			echo "<table class='spelbord'>";
			for ($teller = 1; $teller <=6; $teller++) {
				echo "<tr>";
				for ($teller2 = 1; $teller2 <=7; $teller2++) {
					echo "<td width=50><a href='#'><img style='border-radius: 50%' 
					src='".$speelveld[$teller][$teller2].".png' /></a></td>";
				}
				echo "</tr>";
			}
			echo "</table>";

			$naam_beurt=$_GET['winner'];

			echo "<div class='speler_win'>{$naam_beurt} heeft gewonnen!</div>";
		}


		echo "<div class='spel_menu'>";
		echo "<div class='spelers'><p><img src='2.png'><span>{$_SESSION['username']} </span></p>";
		echo "<span><p><img src='1.png'><span>{$_SESSION['username_two']} </span></p></div>";
		echo "<div class='stop_link'><a href='index.php?page=spel&status=quit' class='button'><img src='cancel.png' width='50' height='50'></a></div>";
		echo "</div>";	
			
	}


?>

	</div>