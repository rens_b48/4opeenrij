	<div id="wrapper">
		<div id="login">
<?php
	if(isset($_SESSION['username'])) {
		header("Location:index.php");
	}
	
	
	$foutmeldingen = array ();
	
	if (empty($_POST)) {
		$verwerken=false;
		$name = $email = $password = null;
	}else{
		$verwerken=true;
		$name=($_POST["naam"]);
		$email=strtolower($_POST["email"]);
		$password=($_POST["wachtwoord"]);

/*
		if (checkUsername($name)) != 0){
			$verwerken=false;
			$foutmeldingen[]="Gebruikersnaam is al in gebruik";
		}

*/		
		if (isset($_POST['check'])) {
			$verwerken=true;
		}else{
			$verwerken=false;
			$foutmeldingen[]="U moet akkoord gaan met de voorwaarden";
		}

		if (mb_strlen($name)<1) {
			$verwerken=false;
			$foutmeldingen[]="Gebruikersnaam invoeren";
		}
		if (mb_strlen($name)>15) {
			$verwerken=false;
			$foutmeldingen[]="Gebruikersnaam is te lang";
		}
		if ($name==usernameIsPresent($name)) {
			$verwerken=false;
			$foutmeldingen[]="Gebruikersnaam is al in gebruik";
		}
		include("email_validatie.php");
		if (mb_strlen($password)<1) {
			$verwerken=false;
			$foutmeldingen[]="Wachtwoord invoeren";
		}
		if (mb_strlen($password)>254) {
			$verwerken=false;
			$foutmeldingen[]="Wachtwoord is te lang";
		}
		$verzend=empty($foutmeldingen);
	}
	
	if($verwerken==true){
		getRegister($name, $email, $password);	
		echo <<<EOT
		</div>
		<div class="succesmelding">
			<h2>Uw account is aangemaakt</h2>
			<a href="index.php">Klik hier om in te loggen!</a>
		</div>
EOT;
		header("refresh:3; url=index.php");
	}else{
		$verwerken=false;

echo <<<EOT
	<h2>Registreren</h2>
		<form method="POST">
			<input type="text" name="naam" id="username" value="{$name}" placeholder="Gebruikersnaam"/>
			<input type="email" name="email" id="email" value="{$email}" placeholder="E-mail"/>
			<input type="password" name="wachtwoord" value="{$password}" placeholder="Wachtwoord"/>
			<input type="checkbox" name="check" value="check"><p>Ik ben 18 jaar en ouder</p>
			<input type="submit" value="Registreer">
		</form>
		<div id="form_links"><a href="index.php">Al geregistreerd?</a></div>
	</div>
EOT;

		foreach($foutmeldingen as $foutmelding) {echo <<<EOT
			<div class="foutmelding">
				{$foutmelding} 
			</div>
EOT;
		}
	}
?>
	
	</div>